import defaultSettings from '@/settings'

const title = defaultSettings.title || 'What\'s App Admin'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
