import request from '@/utils/request'

export function fetchMessageListByAccount(query) {
  query.offset = (query.page / 1 - 1) * query.limit
  return request({
    url: '/contact/' + query.account + '/messages',
    method: 'get',
    params: query
  })
}
