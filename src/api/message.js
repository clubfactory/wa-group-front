import request from '@/utils/request'

export function sendMessage(data) {
  return request({
    url: '/messages',
    method: 'post',
    data
  })
}
