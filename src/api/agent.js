import request from '@/utils/request'

export function fetchAgentList(query) {
  query.offset = (query.page / 1 - 1) * query.limit
  return request({
    url: '/agents',
    method: 'get',
    params: query
  })
}

export function addAgent(data) {
  return request({
    url: '/agents',
    method: 'post',
    data
  })
}

export function editAgent(data) {
  return request({
    url: '/agents/' + data.id,
    method: 'put',
    data
  })
}

export function requestVerifyCode(data) {
  return request({
    url: '/agents/' + data.id + '/request_code',
    method: 'post',
    data
  })
}

export function register(data) {
  return request({
    url: '/agents/' + data.id + '/register',
    method: 'post',
    data
  })
}

export function fetchGroupList(id) {
  return request({
    url: '/agents/' + id + '/groups',
    method: 'get'
  })
}

export function fetchContactList(id) {
  return request({
    url: '/agents/' + id + '/contacts',
    method: 'get'
  })
}

export function syncGroup(id) {
  return request({
    url: '/agents/' + id + '/reqSync',
    method: 'post'
  })
}

export function createGroup(data) {
  return request({
    url: '/agents/' + data.agentId + '/groups',
    method: 'post',
    data
  })
}

const domainPath = process.env.VUE_APP_BASE_API
export { domainPath }
