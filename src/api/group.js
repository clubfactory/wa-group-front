import request from '@/utils/request'

export function fetchMessageListByGroup(query) {
  query.offset = (query.page / 1 - 1) * query.limit
  return request({
    url: '/groups/' + query.groupId + '/messages',
    method: 'get',
    params: query
  })
}
